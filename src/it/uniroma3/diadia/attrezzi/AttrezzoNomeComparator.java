package it.uniroma3.diadia.attrezzi;
import java.util.Comparator;

public class AttrezzoNomeComparator implements Comparator<Attrezzo>{
	
	@Override
	public int compare(Attrezzo attrezzo_1, Attrezzo attrezzo_2) {
		int diffNome = attrezzo_1.compareTo(attrezzo_2);
		if (diffNome==0)
			return attrezzo_1.getPeso() - attrezzo_2.getPeso();
		return diffNome;
	}
}
