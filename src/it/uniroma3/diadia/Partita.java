package it.uniroma3.diadia;

import it.uniroma3.diadia.ambienti.Labirinto;
import it.uniroma3.diadia.giocatore.Giocatore;
import it.uniroma3.diadia.ambienti.Stanza;
/**
 * Questa classe modella una partita del gioco
 *
 * @author docente di POO
 * @see Stanza
 * @version base
 */

public class Partita {

	private boolean finita;
	private Labirinto labirinto;
	private Giocatore giocatore;

	public Partita() {
		creaLabirinto();
		creaGiocatore();
		this.finita = false;
	}

	private void creaGiocatore() {
		this.giocatore = new Giocatore();
	}

	public Giocatore getGiocatore() {
		return this.giocatore;
	}

	private void creaLabirinto() {
		this.labirinto = new Labirinto();
	}

	public Labirinto getLabirinto() {
		return this.labirinto;
	}

	/**
	 * Restituisce vero se e solo se la partita e' stata vinta
	 * 
	 * @return vero se partita vinta
	 */
	public boolean vinta() {
		return this.labirinto.getStanzaCorrente() == this.labirinto.getStanzaVincente();
	}

	/**
	 * Restituisce vero se e solo se la partita e' finita
	 * 
	 * @return vero se partita finita
	 */

	public boolean isFinita() {
		return finita || vinta() || !giocatoreInGioco();
	}

	public boolean giocatoreInGioco() {
		return this.giocatore.getCfu() != 0;
	}

	public void setFinita() {
		this.finita = true;
	}

}