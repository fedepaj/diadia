package it.uniroma3.diadia.giocatore;

import it.uniroma3.diadia.attrezzi.Attrezzo;
import it.uniroma3.diadia.attrezzi.AttrezzoNomeComparator;

import java.util.*;

public class Borsa {
	public final static int DEFAULT_PESO_MAX_BORSA = 10;
	private Map<String, Attrezzo> attrezzi;
	private int pesoMax;

	public Borsa() {
		this(DEFAULT_PESO_MAX_BORSA);
	}

	public Borsa(int pesoMax) {
		this.pesoMax = pesoMax;
		this.attrezzi = new HashMap<String, Attrezzo>();
	}

	public boolean addAttrezzo(Attrezzo attrezzo) {
		if (this.getPeso() + attrezzo.getPeso() > this.getPesoMax())
			return false;
		else {
			this.attrezzi.put(attrezzo.getNome(), attrezzo);
			return true;
		}
	}

	public int getPesoMax() {
		return pesoMax;
	}

	public Attrezzo getAttrezzo(String nomeAttrezzo) {
		if (attrezzi.containsKey(nomeAttrezzo))
			return attrezzi.get(nomeAttrezzo);
		return null;
	}

	// unico ciclo da non togliere, non � di ricerca!!
	public int getPeso() {
		int peso = 0;
		for (Attrezzo attrezzo : attrezzi.values())
			peso += attrezzo.getPeso();
		return peso;
	}

	// --
	public boolean isEmpty() {
		return this.attrezzi.isEmpty();
	}
	// --

	public boolean hasAttrezzo(String nomeAttrezzo) {
		return this.getAttrezzo(nomeAttrezzo) != null;
	}

	public List<Attrezzo> getContenutoOrdinatoPerPeso() {
		List<Attrezzo> attrezziOrdinatiPerPeso = new LinkedList<Attrezzo>(this.attrezzi.values());
		Collections.sort(attrezziOrdinatiPerPeso);
		return attrezziOrdinatiPerPeso;
	}

	public SortedSet<Attrezzo> getContenutoOrdinatoPerNome() {
		SortedSet<Attrezzo> attrezziOrdinatiPerNome = new TreeSet<Attrezzo>(new AttrezzoNomeComparator());
		attrezziOrdinatiPerNome.addAll(this.attrezzi.values());
		return attrezziOrdinatiPerNome;
	}

	public SortedSet<Attrezzo> getSortedSetOrdinatoPerPeso() {
		SortedSet<Attrezzo> attrezziOrdinatiPerPeso = new TreeSet<Attrezzo>();
		attrezziOrdinatiPerPeso.addAll(this.attrezzi.values());
		return attrezziOrdinatiPerPeso;
	}

	public Map<Integer, Set<Attrezzo>> getContenutoRaggruppatoPerPeso() {
		Map<Integer, Set<Attrezzo>> contenutoRaggruppatoPerPeso = new HashMap<>();
		Set<Attrezzo> attrezziconLoStessoPeso = null;
		for (Attrezzo attrezzo : this.attrezzi.values()) {
			if (contenutoRaggruppatoPerPeso.containsKey(new Integer(attrezzo.getPeso())))
				attrezziconLoStessoPeso = contenutoRaggruppatoPerPeso.get(new Integer(attrezzo.getPeso()));
			else
				attrezziconLoStessoPeso = new HashSet<Attrezzo>();
			attrezziconLoStessoPeso.add(attrezzo);
			contenutoRaggruppatoPerPeso.put(new Integer(attrezzo.getPeso()), attrezziconLoStessoPeso);
		}
		return contenutoRaggruppatoPerPeso;
	}

	public Attrezzo removeAttrezzo(String nomeAttrezzo) {
		Attrezzo a = null;
		Iterator<Attrezzo> iteratore = this.attrezzi.values().iterator();
		while (iteratore.hasNext()) {
			a = iteratore.next();
			if (a.getNome().equals(nomeAttrezzo)) {
				iteratore.remove();
				return a;
			}
		}
		return null;
	}

	@Override
	public String toString() {
		StringBuilder s = new StringBuilder();

		if (!this.isEmpty()) {
			s.append("Contenuto borsa (" + this.getPeso() + "kg/" + this.getPesoMax() + "kg): ");
			s.append(attrezzi.values().toString());
		} else
			s.append("La tua borsa � vuota");
		return s.toString();
	}
}