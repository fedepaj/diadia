package it.uniroma3.diadia;
import java.util.Scanner;
import it.uniroma3.diadia.comandi.Comando;
import it.uniroma3.diadia.comandi.FabbricaDiComandiFisarmonica;


/**
 * Classe principale di diadia, un semplice gioco di ruolo ambientato al dia.
 * Per giocare crea un'istanza di questa classe e invoca il letodo gioca
 *
 * Questa e' la classe principale crea e istanzia tutte le altre
 *
 * @author  docente di POO 
 *         (da un'idea di Michael Kolling and David J. Barnes) 
 *          
 * @version base
 */


public class DiaDia {
	private Partita partita;

    public DiaDia() {
    	this.partita = new Partita();
    }
/*
	private static String[] elencoComandi = {"vai", "aiuto", "fine","prendi","borsa","posa"};

	public DiaDia() {
		this.partita = new Partita();
	}
*/
	public void gioca() {
		String istruzione; 



		Scanner scannerDiLinee = new Scanner(System.in);		
		do		
			istruzione = scannerDiLinee.nextLine();
		while (!processaIstruzione(istruzione));
		scannerDiLinee.close();
	}   


	private boolean processaIstruzione(String istruzione) {
		Comando comandoDaEseguire;
		
		FabbricaDiComandiFisarmonica factory = new FabbricaDiComandiFisarmonica();
		
		comandoDaEseguire = factory.costruisciComando(istruzione);
		
		comandoDaEseguire.esegui(this.partita);
		
		if(this.partita.vinta())
			System.out.println("Hai Vinto!");
		if(!(this.partita.giocatoreInGioco()))
			System.out.println("CFU esauriti...");
		return this.partita.isFinita();
	}


	/*private void finecfu() {
		System.out.println("Hai finito i tentativi !");
	} 
	private void fine() {
		System.out.println("Grazie di aver giocato!");  // si desidera smettere
	} */

	public static void main(String[] argc) {


		DiaDia gioco = new DiaDia();
		gioco.gioca();


	}
}