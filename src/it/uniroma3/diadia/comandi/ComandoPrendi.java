package it.uniroma3.diadia.comandi;
import it.uniroma3.diadia.Partita;


public class ComandoPrendi implements Comando {
	
	private String nomeAttrezzo;
	@Override
	public void esegui(Partita partita) {
		
	

		
		if(partita.getLabirinto().getStanzaCorrente().hasAttrezzo(nomeAttrezzo)){
		
		
	if(partita.getGiocatore().getBorsa().addAttrezzo(partita.getLabirinto().getStanzaCorrente().getAttrezzo(nomeAttrezzo))) {
				
			partita.getLabirinto().getStanzaCorrente().removeAttrezzo(partita.getLabirinto().getStanzaCorrente().getAttrezzo(nomeAttrezzo));
			System.out.println("Hai preso "+nomeAttrezzo);
		}
		else System.out.println("Non hai spazio nella borsa!");
	}
	else System.out.println("Attrezzo non presente nella stanza");
}

@Override
public void setParametro(String parametro) {
	this.nomeAttrezzo=parametro;
}
}