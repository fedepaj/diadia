package it.uniroma3.diadia.comandi;

import it.uniroma3.diadia.Partita;


public class ComandoGuarda implements Comando {
	
	@Override
	
	public void esegui(Partita partita) {
		System.out.println(partita.getLabirinto().getStanzaCorrente().getDescrizione());
	}

	@Override
	public void setParametro(String parametro) {
	}
	
}
