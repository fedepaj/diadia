package it.uniroma3.diadia.comandi;

import it.uniroma3.diadia.comandi.Comando;
import it.uniroma3.diadia.Partita;
import it.uniroma3.diadia.ambienti.Stanza;


public class ComandoVai implements Comando {
private String direzione;


@Override 
public void esegui(Partita partita) {

	if(direzione==null)
		System.out.println("Dove vuoi andare ?");
	Stanza prossimaStanza = null;
	prossimaStanza = partita.getLabirinto().getStanzaCorrente().getStanzaAdiacente(direzione);
	if (prossimaStanza == null)
		System.out.println("Direzione inesistente");
	else {
		partita.getLabirinto().setStanzaCorrente(prossimaStanza);
		partita.getGiocatore().setCfu(partita.getGiocatore().getCfu()-1);
	}
	System.out.println(partita.getLabirinto().getStanzaCorrente().getDescrizione());
	System.out.println("Tentativi rimasti :"+partita.getGiocatore().getCfu()); /* controllo sui cfu che non diminuiscono */
}


public void setParametro(String parametro) {
	this.direzione=parametro;
}
}
