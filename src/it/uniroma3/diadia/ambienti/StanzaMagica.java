package it.uniroma3.diadia.ambienti;
import it.uniroma3.diadia.ambienti.Stanza;
import it.uniroma3.diadia.attrezzi.Attrezzo;

public class StanzaMagica extends Stanza{
	final static private int SOGLIA_DEFAULT = 3; 
	private int sogliaMagica;
	private int contatoreAttrezziPrelevati;

	public StanzaMagica(String nome) {
		this(nome, SOGLIA_DEFAULT);
	}
	
	public StanzaMagica(String nome, int soglia){
		super(nome);
		this.sogliaMagica = soglia;
		this.contatoreAttrezziPrelevati = 0;
	}
	
	private Attrezzo modificaAttrezzo(Attrezzo attrezzo){
		StringBuilder nomeInvertito = new StringBuilder(attrezzo.getNome()).reverse();
		return new Attrezzo(nomeInvertito.toString(), attrezzo.getPeso()*2);
	}
	
	
	@Override
	public boolean addAttrezzo(Attrezzo attrezzo){
		if (this.contatoreAttrezziPrelevati > this.sogliaMagica)
			attrezzo = this.modificaAttrezzo(attrezzo);
		return super.addAttrezzo(attrezzo);
	}
	
	
	@Override
	public boolean removeAttrezzo(Attrezzo attrezzo) {
		this.contatoreAttrezziPrelevati++;
		return super.removeAttrezzo(attrezzo);
	}

}