package it.uniroma3.diadia.ambienti;

import it.uniroma3.diadia.attrezzi.Attrezzo;


public final class StanzaMagicaProtected extends StanzaProtected{
	final static private int SOGLIA_DEFAULT = 3; 
	private int sogliaMagica;
	
	private int contatoreAttrezziPosati;
	private int numeroAttrezzi;
	private int nomeroAttrezzi;

	public StanzaMagicaProtected(String nome) {
		this(nome, SOGLIA_DEFAULT);
	}
	
	public StanzaMagicaProtected(String nome, int soglia){
		super(nome);
		this.sogliaMagica = soglia;
		this.numeroAttrezzi = 0;
	}
	
	private Attrezzo modificaAttrezzo(Attrezzo attrezzo){
		StringBuilder nomeInvertito = new StringBuilder(attrezzo.getNome()).reverse();
		return new Attrezzo(nomeInvertito.toString(), attrezzo.getPeso()*2);	
	}
	
	/**
     * Mette un attrezzo nella stanza.
     * @param attrezzo l'attrezzo da mettere nella stanza.
     * @return true se riesce ad aggiungere l'attrezzo, false atrimenti.
     */
	@Override
	public boolean addAttrezzo(Attrezzo attrezzo) {
		this.contatoreAttrezziPosati++;
		if (this.contatoreAttrezziPosati > this.sogliaMagica)
		attrezzo = this.modificaAttrezzo(attrezzo);
		if (this.numeroAttrezzi<this.attrezzi.length) {
		this.attrezzi[this.nomeroAttrezzi] = attrezzo;
		this.nomeroAttrezzi++;
		return true;

		}
		else return false;
	}
	
	/**
	 * Rimuove un attrezzo dalla stanza (ricerca in base al nome).
	 * @param attrezzo Contiene l'oggetto da rimuovere
	 * @return true se l'attrezzo e' stato rimosso, false altrimenti
	 */
	//@Override
	//public boolean removeAttrezzo(Attrezzo attrezzo) {
	//	this.contatoreAttrezziPrelevati++;
	//	return this.attrezzi.remove(attrezzo.getNome()) != null;
	//}
}