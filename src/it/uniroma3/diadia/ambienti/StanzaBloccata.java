package it.uniroma3.diadia.ambienti;

public class StanzaBloccata extends Stanza{
	private static final String DEFAULT_DIREZIONE = "nord";
	private static final String DEFAULT_CHIAVE = "chiave";
	private String direzioneNascosta;
	private String nomeChiave;
	
	public StanzaBloccata(String nome){
		this(nome, DEFAULT_DIREZIONE, DEFAULT_CHIAVE);
	}
	
	public StanzaBloccata(String nome, String direzioneNascosta, String nomeChiave){
		super(nome);
		this.nomeChiave = nomeChiave;
		this.direzioneNascosta = direzioneNascosta;
	}
	
	/**
     * Restituisce la descrizione della stanza.
     * @return la descrizione della stanza
     */
	@Override
	public String getDescrizione(){
		if (!this.hasAttrezzo(this.nomeChiave))
			return "Qui qualcosa non va, le porte non sono 'aporte'";
		else return super.getDescrizione();
	}
	
	/**
     * Restituisce la stanza adiacente nella direzione specificata
     * @param direzione la direzione
     */
	@Override
	public Stanza getStanzaAdiacente(String direzione) {
		if(direzione != null) {
			if (direzione.equals(this.direzioneNascosta) && !this.hasAttrezzo(this.nomeChiave)){
				return this;
			}
		}
		return super.getStanzaAdiacente(direzione);
	}
	
}