package it.uniroma3.diadia.ambienti;

public class StanzaBuia extends Stanza{
	private static String DEFAULT_NOME_INTERRUTTORE = "lanterna";
	private String nomeInterruttore;
	
	public StanzaBuia(String nome){
		this(nome, DEFAULT_NOME_INTERRUTTORE);
	}
	
	public StanzaBuia(String nome, String nomeInterruttore){
		super(nome);
		this.nomeInterruttore = nomeInterruttore;
	}
	
	@Override
	public String getDescrizione(){
		if (super.hasAttrezzo(this.nomeInterruttore))
			return super.getDescrizione();
		return "Qui c'� un buio pesto!";
	}

}
