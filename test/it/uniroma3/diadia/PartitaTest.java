package it.uniroma3.diadia;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import it.uniroma3.diadia.ambienti.*;

public class PartitaTest {
	
	private Partita partita_test;
	
	@Before
	public void setUp() throws Exception {
		this.partita_test = new Partita();
	}
	//TODO
	@Test
	public void testGetStanzaCorrente() {
		Stanza stanzaTest = new Stanza("stanzaTest");
		partita_test.getLabirinto().setStanzaCorrente(stanzaTest);
		assertSame(stanzaTest,partita_test.getLabirinto().getStanzaCorrente());
	}
	
	@Test
	public void testGetStanzaCorrente_maSetStanzaDiversa() {	
		Stanza stanzaTest_0 = new Stanza("stanzaTest_0");
		Stanza stanzaTest_1 = new Stanza("stanzaTest_1");
		partita_test.getLabirinto().setStanzaCorrente(stanzaTest_0);
		assertNotSame(stanzaTest_1,partita_test.getLabirinto().getStanzaCorrente());
	}
	
	@Test
	public void testGetCfu() {		//usa setCfu
		this.partita_test.getGiocatore().setCfu(1);
		assertEquals(1,this.partita_test.getGiocatore().getCfu());
	}
	
	@Test
	public void testVinta() {		//usa setStanzaCorrente
		this.partita_test.getLabirinto().setStanzaCorrente(this.partita_test.getLabirinto().getStanzaVincente());
		assertTrue(partita_test.vinta());
	}
	
	@Test
	public void testVinta_maNonIsVinta() {		//usa setStanzaCorrente
		Stanza stanza_test = new Stanza("stanza_test");
		this.partita_test.getLabirinto().setStanzaCorrente(stanza_test);
		assertFalse(partita_test.vinta());
	}

	@Test
	public void testIsFinita_perCfuPariaZero() { 	//usa setFinita
		this.partita_test.getGiocatore().setCfu(0);
		assertTrue(this.partita_test.isFinita());
	}
	
	@Test
	public void testIsFinita_perVintaReturnedTrue() { 	//usa setFinita
		this.partita_test.getLabirinto().setStanzaCorrente(this.partita_test.getLabirinto().getStanzaVincente());
		assertTrue(this.partita_test.isFinita());	
	}
	
	@Test
	public void testIsFinita_perVariabileFinitaSetTrue() { 	//usa setFinita
		this.partita_test.setFinita();
		assertTrue(this.partita_test.isFinita());	
	}

}
