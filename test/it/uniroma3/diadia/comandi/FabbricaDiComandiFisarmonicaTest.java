package it.uniroma3.diadia.comandi;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class FabbricaDiComandiFisarmonicaTest {
	private FabbricaDiComandiFisarmonica factory;
	/*
	if (nomeComando == null) {
		comando = new ComandoNonValido();
		return comando;		
	}
		ComandoVai();
	else if(nomeComando.equals("guarda"))
		comando = new ComandoGuarda();
	else if(nomeComando.equals("prendi"))
		comando = new ComandoPrendi();
	else if(nomeComando.equals("aiuto"))
		comando = new ComandoAiuto();
	else if(nomeComando.equals("fine"))
		comando = new ComandoFine();
	else if(nomeComando.equals("posa"))
		comando = new ComandoPosa();
	else if(nomeComando.equals("borsa"))
		comando = new ComandoBorsa();
	*/
	@Test
	public void testCostruisciComando_aiuto() {
		ComandoAiuto aiuto = new ComandoAiuto();
		this.factory = new FabbricaDiComandiFisarmonica();
		assertEquals(factory.costruisciComando("aiuto"), aiuto);
	}

}
