package it.uniroma3.diadia.comandi;
import it.uniroma3.diadia.*;
import it.uniroma3.diadia.comandi.ComandoFine;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class ComandoFineTest {
	private Partita gioco;
	private ComandoFine comandoFine;
	
	@Before
	public void setUp() throws Exception {
		this.gioco = new Partita();
		this.comandoFine = new ComandoFine();
	}

	@Test
	public void testEsegui() {
		this.comandoFine.esegui(gioco);
		assertTrue(this.gioco.isFinita());
	}
}
