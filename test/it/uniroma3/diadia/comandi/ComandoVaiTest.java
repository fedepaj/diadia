package it.uniroma3.diadia.comandi;
import  it.uniroma3.diadia.ambienti.Stanza;
import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import it.uniroma3.diadia.Partita;

public class ComandoVaiTest {
	private Partita gioco;
	private ComandoVai comandoVai;
	private Stanza stanzaTest;
	private Stanza stanzaTest_adiacente;
	
	@Before
	public void setUp() throws Exception {
		this.gioco = new Partita();
		this.comandoVai = new ComandoVai();
		this.stanzaTest = new Stanza("stanzaTest");
		this.stanzaTest_adiacente = new Stanza("stanzaTest_adiacente");
		this.gioco.getLabirinto().setStanzaCorrente(stanzaTest);
		stanzaTest.impostaStanzaAdiacente("est", stanzaTest_adiacente);
	}
	@Test
	public void testEsegui_stanzaEsistenteInDirezione() {
		//this.gioco.getLabirinto().getStanzaCorrente().getStanzaAdiacente("est");
		this.comandoVai.setParametro("est");
		this.comandoVai.esegui(gioco);
		assertSame(this.gioco.getLabirinto().getStanzaCorrente(), this.stanzaTest_adiacente);
	}

	@Test
	public void testEsegui_stanzaNonEsistenteInDirezione() {
		//this.gioco.getLabirinto().getStanzaCorrente().getStanzaAdiacente("est");
		this.comandoVai.setParametro("ovest");
		this.comandoVai.esegui(gioco);
		assertSame(this.gioco.getLabirinto().getStanzaCorrente(), this.stanzaTest);
	}
	
	@Test
	public void testEsegui_comandoSenzaParametro() {
		//this.gioco.getLabirinto().getStanzaCorrente().getStanzaAdiacente("est");
		this.comandoVai.esegui(gioco);
		assertSame(this.gioco.getLabirinto().getStanzaCorrente(), this.stanzaTest);
	}
	
	@Test
	public void testEsegui_comandoConParameetroStringaVuota() {
		//this.gioco.getLabirinto().getStanzaCorrente().getStanzaAdiacente("est");
		this.comandoVai.setParametro("");
		this.comandoVai.esegui(gioco);
		assertSame(this.gioco.getLabirinto().getStanzaCorrente(), this.stanzaTest);
	}
}
