package it.uniroma3.diadia.comandi;
import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import it.uniroma3.diadia.Partita;
import it.uniroma3.diadia.ambienti.Stanza;
import it.uniroma3.diadia.attrezzi.Attrezzo;
import it.uniroma3.diadia.comandi.ComandoPrendi;


public class ComandoPrendiTes {

	
	Partita partita;
	ComandoPrendi comP;
	Attrezzo torcia;
	Stanza stanza;
	
	@Before
	public void setUp() throws Exception{
		
		
		partita = new Partita();
		comP = new ComandoPrendi();
		torcia = new Attrezzo("torcia", 2);
		stanza = new Stanza("corridoio");
		partita.getLabirinto().setStanzaCorrente(stanza);
		partita.getLabirinto().getStanzaCorrente().addAttrezzo(torcia);
	}
	
	
	
	@Test
	public void testEsegui_AttrezzoNull() {
		comP.setParametro(null);
		comP.esegui(partita);
		assertSame(torcia, this.partita.getLabirinto().getStanzaCorrente().getAttrezzo("torcia"));
	}

	@Test
	public void testEsegui_AttrezzoNonPresenteNellaStanza() {
		comP.setParametro("casa");
		comP.esegui(partita);
		assertSame(torcia, this.partita.getLabirinto().getStanzaCorrente().getAttrezzo("torcia"));
	}
	
	@Test
	public void testEsegui_NessuAttrezzoNellaStanza() {
		Stanza vuota = new Stanza("vuota");
		this.partita.getLabirinto().setStanzaCorrente(vuota);
		
		comP.setParametro("torcia");
		comP.esegui(partita);
		assertSame(vuota, this.partita.getLabirinto().getStanzaCorrente());
	}
	
	@Test
	public void testEsegui_BorsaPiena() {
		Attrezzo peso = new Attrezzo("peso", 10);
		this.partita.getGiocatore().getBorsa().addAttrezzo(peso);
		
		comP.setParametro("torcia");
		comP.esegui(partita);
		assertSame(torcia, this.partita.getLabirinto().getStanzaCorrente().getAttrezzo("torcia"));
	}
}