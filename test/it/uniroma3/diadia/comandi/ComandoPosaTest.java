package it.uniroma3.diadia.comandi;
import it.uniroma3.diadia.ambienti.Stanza;
import it.uniroma3.diadia.attrezzi.Attrezzo;
import it.uniroma3.diadia.giocatore.Borsa;
import it.uniroma3.diadia.comandi.*;
import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import it.uniroma3.diadia.Partita;

public class ComandoPosaTest {
	private Partita gioco;
	private Borsa borsaTest;
	private Stanza stanzaTest;
	private ComandoPosa comandoPosa;
	private Attrezzo attrezzoTest;
	
	@Before
	public void setUp() throws Exception {
		this.gioco = new Partita();
		this.borsaTest = new Borsa();
		this.stanzaTest = new Stanza("stanzaTest");
		this.comandoPosa = new ComandoPosa();
		this.attrezzoTest = new Attrezzo("attrezzoTest", 1);
	}
	/**TO-DO implementa sto test con successo*/
	@Test
	public void testEsegui_borsaConUnOggetto() {
		this.gioco.getLabirinto().setStanzaCorrente(stanzaTest);
		this.gioco.getGiocatore().getBorsa().addAttrezzo(attrezzoTest);
		this.comandoPosa.setParametro("attrezzoTest");
		this.comandoPosa.esegui(gioco);
		assertTrue(this.gioco.getGiocatore().getBorsa().isEmpty());
	}

	@Test
	public void testEsegui_borsaConDueOggettiUguali() {
		Attrezzo attrezzoTest_2 = new Attrezzo("attrezzoTest", 1);
		this.borsaTest.addAttrezzo(attrezzoTest);
		this.borsaTest.addAttrezzo(attrezzoTest_2);
		this.comandoPosa.setParametro("attrezzoTest");
		this.comandoPosa.esegui(gioco);
		assertTrue(this.borsaTest.hasAttrezzo("attrezzoTest"));
	}
	
	@Test
	public void testEsegui_borsaSenzaOggetti() {
		this.comandoPosa.setParametro("attrezzoTest");
		this.comandoPosa.esegui(gioco);
		assertTrue(this.borsaTest.isEmpty());
	}
}
