package it.uniroma3.diadia.giocatore;

import static org.junit.Assert.*;
import it.uniroma3.diadia.attrezzi.Attrezzo;
import it.uniroma3.diadia.giocatore.Borsa;

import org.junit.Before;
import org.junit.Test;

public class BorsaTest {
	private Borsa borsaVuota;
	private String nomeSoldi;
	private Borsa borsaConSoldiGomme;
	private Attrezzo soldi;
	private Attrezzo gomme;

	@Before
	public void setUp() throws Exception {
		this.borsaVuota = new Borsa();
		this.nomeSoldi = new String("soldi");
		this.soldi = new Attrezzo("soldi", 1);
		this.gomme = new Attrezzo("gomme", 2);
		this.borsaConSoldiGomme = new Borsa();
		this.borsaConSoldiGomme.addAttrezzo(soldi);
		this.borsaConSoldiGomme.addAttrezzo(gomme);

	}

	@Test
	public void testRemoveAttrezzoDaBorsaVuota() {
		assertTrue(this.borsaVuota.isEmpty());
		Attrezzo a = this.borsaVuota.removeAttrezzo(this.nomeSoldi);
		assertNull(a);
		assertTrue(this.borsaVuota.isEmpty());

	}

	@Test
	public void testRemoveAttrezzoPresente() {
		assertFalse(this.borsaConSoldiGomme.isEmpty());
		Attrezzo a = this.borsaConSoldiGomme.removeAttrezzo("penne");
		assertFalse(this.borsaConSoldiGomme.isEmpty());
		assertEquals(this.borsaConSoldiGomme.getPeso(), 3);
		assertNull(a);
	}

	@Test
	public void testRemoveAttrezzoParametroNull() {
		assertFalse(this.borsaConSoldiGomme.isEmpty());
		assertTrue(this.borsaVuota.isEmpty());
		Attrezzo a = this.borsaConSoldiGomme.removeAttrezzo(null);
		assertFalse(this.borsaConSoldiGomme.isEmpty());
		assertEquals(this.borsaConSoldiGomme.getPeso(), 3);
		assertNull(a);
		a = this.borsaVuota.removeAttrezzo(null);
		assertTrue(this.borsaVuota.isEmpty());
		assertEquals(this.borsaVuota.getPeso(), 0);
	}

		//TODO fai test getSortedOrdinatiPerPeso
	
	@Test
	public void testRemoveAttrezzoNonPresente() {
		assertFalse(this.borsaConSoldiGomme.isEmpty());
		Attrezzo a = this.borsaConSoldiGomme.removeAttrezzo("gomme");
		assertFalse(this.borsaConSoldiGomme.isEmpty());
		assertEquals(this.borsaConSoldiGomme.getPeso(), 1);
		assertSame(a, this.gomme);
	}
	
	@Test
	public void testgetContenutoOrdinatoPerPeso() {
		
	}
	
	@Test
	public void testgetContenutoOrdinatoPerNome() {
		
	}
	
	@Test
	public void testgetSortedSetOrdinatoPerPeso() {
		
	}
	
	@Test
	public void testgetContenutoRaggruppatoPerPeso_dueOggettiConLoStessoNome() {
		
	}
	
	
}
