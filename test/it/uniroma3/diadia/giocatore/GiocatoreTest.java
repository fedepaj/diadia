package it.uniroma3.diadia.giocatore;

import static org.junit.Assert.*;
import it.uniroma3.diadia.giocatore.Borsa;
import it.uniroma3.diadia.giocatore.Giocatore;

import org.junit.Before;
import org.junit.Test;

public class GiocatoreTest {
	private int CFU;
	private Borsa borsa;
	private Giocatore giocatore;
	
	@Before
	public void setUp() throws Exception {
		this.CFU= 20;
		this.borsa = new Borsa();
		this.giocatore = new Giocatore();
	}
	@Test
	public void testGetBorsa() {
		assertEquals(this.borsa.toString(),this.giocatore.getBorsa().toString());
	}
	@Test
	public void testGetCFU() {
		assertSame(20, this.CFU);
	}
	@Test
	public void testSetCFU(){
		this.giocatore.setCfu(10);
		assertSame(10,this.giocatore.getCfu());
	}

}
