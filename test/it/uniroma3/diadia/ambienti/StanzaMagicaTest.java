package it.uniroma3.diadia.ambienti;

import it.uniroma3.diadia.attrezzi.Attrezzo;
import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class StanzaMagicaTest {
	private StanzaMagica stanzaMagicaTest;
	private Attrezzo attrezzoTest;
	
	@Before
	public void setUp() throws Exception {
		stanzaMagicaTest = new StanzaMagica("stanzaMagicaTest", 0);
		attrezzoTest = new Attrezzo("attrezzoTest", 1);
	}

	@Test
	public void testAddAttrezzo_senzaMagia() {
		assertTrue(stanzaMagicaTest.addAttrezzo(attrezzoTest));
	}

	@Test
	public void testAddAttrezzo_conMagia() {
		stanzaMagicaTest.addAttrezzo(attrezzoTest);
		stanzaMagicaTest.removeAttrezzo(attrezzoTest);
		stanzaMagicaTest.addAttrezzo(attrezzoTest);
		assertEquals(stanzaMagicaTest.getAttrezzi()[0].getNome(), "tseTozzertta");
	}
	
	@Test
	public void testRemoveAttrezzo_conAttrezzo() {
		stanzaMagicaTest.addAttrezzo(attrezzoTest);
		assertTrue(stanzaMagicaTest.removeAttrezzo(attrezzoTest));

	}
	
	@Test
	public void testRemoveAttrezzo_senzaAttrezzo() {
		assertFalse(stanzaMagicaTest.removeAttrezzo(attrezzoTest));
	}
}
