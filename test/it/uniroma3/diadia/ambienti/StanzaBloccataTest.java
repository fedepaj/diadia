package it.uniroma3.diadia.ambienti;
import it.uniroma3.diadia.attrezzi.Attrezzo;
import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class StanzaBloccataTest {
	private Attrezzo attrezzoChiaveTest;
	private StanzaBloccata stanzaBloccataTest;
	private Stanza stanzaAdiacenteTest;
	@Before
	public void setUp() throws Exception {
		attrezzoChiaveTest = new Attrezzo("attrezzoChiaveTest", 1);
		stanzaBloccataTest = new StanzaBloccata("stanzaBloccataTest", "est", "attrezzoChiaveTest");
		stanzaAdiacenteTest = new Stanza("stanzaAdiacenteTest");
		stanzaBloccataTest.impostaStanzaAdiacente("est", stanzaAdiacenteTest);
	}

	@Test
	public void testGetDescrizione_conOggettoInterruttore() {
		this.stanzaBloccataTest.addAttrezzo(attrezzoChiaveTest);
		assertNotEquals(stanzaBloccataTest.getDescrizione(), "Qui qualcosa non va, le porte non sono 'aporte'");
	}
	
	@Test
	public void testGetDescrizione_senzaOggettoInterruttore() {
		assertEquals(stanzaBloccataTest.getDescrizione(), "Qui qualcosa non va, le porte non sono 'aporte'");
	}
	
	@Test
	public void testGetStanzaAdiacente_conOggettoInterruttore() {
		this.stanzaBloccataTest.addAttrezzo(attrezzoChiaveTest);
		assertNotSame(this.stanzaBloccataTest.getStanzaAdiacente("est"), this.stanzaBloccataTest);
	}
	
	@Test
	public void testGetStanzaAdiacente_senzaOggetto() {
		assertSame(this.stanzaBloccataTest.getStanzaAdiacente("est"), this.stanzaBloccataTest);
	}
	
	@Test
	public void testGetStanzaAdiacente_conOggettoNonInterruttore() {
		Attrezzo attrezzoNonInterruttoreTest = new Attrezzo("attrezzoNonChiaveTest", 1);
		this.stanzaBloccataTest.addAttrezzo(attrezzoNonInterruttoreTest);
		assertSame(this.stanzaBloccataTest.getStanzaAdiacente("est"), this.stanzaBloccataTest);
	}

}
