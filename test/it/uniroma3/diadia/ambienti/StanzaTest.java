package it.uniroma3.diadia.ambienti;


import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import it.uniroma3.diadia.attrezzi.*;

public class StanzaTest {
	private Stanza stanzaTest_0;
	private Stanza stanzaTest_1;
	private Stanza stanzaTest_2;
	
	@Before
	public void setUp() throws Exception {
		this.stanzaTest_0 = new Stanza("stanzaTest_0");
		this.stanzaTest_1 = new Stanza("stanzaTest_1");
		this.stanzaTest_2 = new Stanza("stanzaTest_2");
	}

	@Test
	public void testGetStanzaAdiacente_conStanzaEsistente() {
		this.stanzaTest_0.impostaStanzaAdiacente("est", this.stanzaTest_1);
		assertSame(this.stanzaTest_1,this.stanzaTest_0.getStanzaAdiacente("est"));
	}
	
	@Test
	public void testGetStanzaAdiacente_conStanzaNonEsistente() {
		this.stanzaTest_0.impostaStanzaAdiacente("est", this.stanzaTest_2);
		assertNotSame(this.stanzaTest_1,this.stanzaTest_0.getStanzaAdiacente("est"));
	}
	

	@Test
	public void testGetNome() {
		assertTrue(stanzaTest_0.getNome().equals("stanzaTest_0"));
	}

	@Test
	public void testGetAttrezzi_conDueAttrezziNellaStanza() {
		Attrezzo osso = new Attrezzo("osso", 3);
		Attrezzo ascia = new Attrezzo("ascia", 1);
		this.stanzaTest_0.addAttrezzo(osso);
		this.stanzaTest_0.addAttrezzo(ascia);
		System.out.println(this.stanzaTest_0.getAttrezzi()[0].getNome());
		assertTrue(this.stanzaTest_0.getAttrezzi()[1].equals(ascia));	//usare equals
		assertTrue(this.stanzaTest_0.getAttrezzi()[0].equals(osso));	//usare equals
		}
	
	@Test
	public void testGetAttrezzi_senzaAttrezziNellaStanza() {
		assertNull(stanzaTest_0.getAttrezzi()[0]);	//usare equals
	}

	@Test
	public void testAddAttrezzo() {
		Attrezzo attrezzo = new Attrezzo("osso", 13);
		assertTrue(this.stanzaTest_0.addAttrezzo(attrezzo));
	}
	
	@Test
	public void testAddAttrezzo_inNumeroMaggiorediLunghezzaArray() {
		for(int i=0;i<=10;i++) {
			Attrezzo attrezzo = new Attrezzo("inseribile" + i, 1);
			this.stanzaTest_0.addAttrezzo(attrezzo);
		}
		Attrezzo attrezzo = new Attrezzo("nonInseribile", 1);
		assertFalse(this.stanzaTest_0.addAttrezzo(attrezzo));
	}

	@Test
	public void testHasAttrezzo_attrezzoEsistente() {
		Attrezzo attrezzo = new Attrezzo("osso", 13);
		this.stanzaTest_0.addAttrezzo(attrezzo);
		assertTrue(this.stanzaTest_0.hasAttrezzo("osso"));
	}
	
	@Test
	public void testHasAttrezzo_attrezzoInsesistente() {
		Attrezzo attrezzo = new Attrezzo("osso", 13);
		this.stanzaTest_0.addAttrezzo(attrezzo);
		assertFalse(this.stanzaTest_0.hasAttrezzo("ascia"));
	}

	@Test
	public void testHasAttrezzo_dueAttrezziUguali() {
		Attrezzo attrezzo = new Attrezzo("osso", 13);
		this.stanzaTest_0.addAttrezzo(attrezzo);
		attrezzo = new Attrezzo("osso", 13);
		this.stanzaTest_0.addAttrezzo(attrezzo);
		assertTrue(this.stanzaTest_0.hasAttrezzo("osso"));
	}
	
	@Test
	public void testGetAttrezzo_conAttrezzoEsistente() {
		Attrezzo attrezzo = new Attrezzo("osso", 13);
		this.stanzaTest_0.addAttrezzo(attrezzo);
		assertSame(attrezzo, this.stanzaTest_0.getAttrezzo("osso"));
	}
	
	@Test
	public void testGetAttrezzo_conAttrezzoNonEsistente() {
		assertNull(this.stanzaTest_0.getAttrezzo("osso"));
	}

	@Test
	public void testRemoveAttrezzo_conUnAttrezzoNellaStanza() {
		Attrezzo attrezzo = new Attrezzo("osso", 13);
		this.stanzaTest_0.addAttrezzo(attrezzo);
		assertTrue(this.stanzaTest_0.removeAttrezzo(attrezzo));
	}
	
	@Test
	public void testRemoveAttrezzo_senzaNessunAttrezzoNellaStanza() {
		Attrezzo attrezzo = new Attrezzo("osso", 13);
		assertFalse(this.stanzaTest_0.removeAttrezzo(attrezzo));
	}
	
	@Test
	public void testRemoveAttrezzo_conDueAttrezziConLoStessoNome() {
		Attrezzo attrezzo = new Attrezzo("osso", 13);
		this.stanzaTest_0.addAttrezzo(attrezzo);
		attrezzo = new Attrezzo("osso", 13);
		this.stanzaTest_0.addAttrezzo(attrezzo);
		assertTrue(this.stanzaTest_0.removeAttrezzo(attrezzo));
	}

	@Test
	public void testGetDirezioni_conUnaDirezione() {
		this.stanzaTest_0.impostaStanzaAdiacente("est", this.stanzaTest_1);
		assertEquals("est", this.stanzaTest_0.getDirezioni()[0]);
	}
	
	@Test
	public void testGetDirezioni_conPiuDirezioni() {
		this.stanzaTest_0.impostaStanzaAdiacente("est", this.stanzaTest_1);
		this.stanzaTest_0.impostaStanzaAdiacente("ovest", this.stanzaTest_2);
		assertEquals("ovest", this.stanzaTest_0.getDirezioni()[1]);
	}
	
	@Test
	public void testGetDirezioni_conNessunaDirezione() {
		assertEquals(0, this.stanzaTest_0.getDirezioni().length);
	}
	
	@Test
	public void testGetDirezioni_conDirezioneDiStanzaAdiacente() {
		this.stanzaTest_0.impostaStanzaAdiacente("est", this.stanzaTest_1);
		this.stanzaTest_1.impostaStanzaAdiacente("est", this.stanzaTest_2);
		assertEquals("est", this.stanzaTest_0.getStanzaAdiacente("est").getDirezioni()[0]);
	}

}
