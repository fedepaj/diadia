package it.uniroma3.diadia.ambienti;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import it.uniroma3.diadia.attrezzi.Attrezzo;

public class StanzaBuiaTest {
	private StanzaBuia stanzaBuiaTest;
	private Attrezzo attrezzoInterruttoreTest;
	
	@Before
	public void setUp() throws Exception {
		attrezzoInterruttoreTest = new Attrezzo("attrezzoInterruttoreTest", 1);
		stanzaBuiaTest = new StanzaBuia("stanzaBuiaTest", "attrezzoInterruttoreTest");
	}

	@Test
	public void testGetDescrizione_conOggettoInterruttore() {
		this.stanzaBuiaTest.addAttrezzo(attrezzoInterruttoreTest);
		assertNotEquals(stanzaBuiaTest.getDescrizione(), "Qui c'è un buio pesto!");
	}
	
	@Test
	public void testGetDescrizione_senzaOggettoInterruttore() {
		assertEquals(stanzaBuiaTest.getDescrizione(), "Qui c'è un buio pesto!");
	}

}
